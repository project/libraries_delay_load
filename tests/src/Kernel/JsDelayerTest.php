<?php

namespace Drupal\Tests\libraries_delay_load\Kernel;

use Drupal\Component\Utility\Crypt;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the JsDelayer service.
 *
 * @group libraries_delay_load
 */
class JsDelayerTest extends KernelTestBase {

  /**
   * The service under test.
   *
   * @var \Drupal\libraries_delay_load\JsDelayer
   */
  protected $jsDelayer;

  /**
   * The entity storage for JS Delay Load config entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $jsDelayLoadStorage;

  /**
   * The file system interface.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The path in front of the js/.
   *
   * @var string
   */
  protected $jsPath;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'libraries_delay_load', 'libraries_delay_load_javascript_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig(['libraries_delay_load']);

    $this->jsDelayer = \Drupal::service('libraries_delay_load.delayer.js');

    $this->jsDelayLoadStorage = $this->container->get('entity_type.manager')->getStorage('js_delay_strategy');
    $this->fileSystem = $this->container->get('file_system');

    $this->jsPath = drupal_get_path('module', 'libraries_delay_load_javascript_test') . '/js/';
  }

  /**
   * Test that the module is enabled by default.
   */
  public function testIsEnabled() {
    $isEnabled = $this->jsDelayer->isEnabled();
    $this->assertTrue($isEnabled);
  }

  /**
   * Test the standard "Global" behavior.
   */
  public function testStandardBehavior() {
    $this->setupStrategiesConfig1();

    $inputJavascript = $this->getInputJavascript();

    $resultJavascript = $inputJavascript;
    $this->jsDelayer->processAssetArray($resultJavascript);

    // Testing that excluded / delayed js are properly removed from the list.
    // Default strategy 1 should be applied.
    // From excluded.
    $this->assertArrayHasKey($this->jsPath . 'js1.js', $inputJavascript);
    $this->assertArrayNotHasKey($this->jsPath . 'js1.js', $resultJavascript);

    // And from various groups.
    $this->assertArrayHasKey($this->jsPath . 'js8.js', $inputJavascript);
    $this->assertArrayNotHasKey($this->jsPath . 'js8.js', $resultJavascript);
    $this->assertArrayHasKey($this->jsPath . 'js5.js', $inputJavascript);
    $this->assertArrayNotHasKey($this->jsPath . 'js5.js', $resultJavascript);

    // Check that some values are still kept.
    $randomJs = $this->jsPath . 'js_random.js';
    $this->assertArrayHasKey($randomJs, $resultJavascript);
    $this->assertArrayHasKey($this->jsPath . 'js6.js', $resultJavascript);
    $selfJs = drupal_get_path('module', 'libraries_delay_load') . '/js/delayer.js';
    $this->assertArrayHasKey($selfJs, $resultJavascript);

    // Check that a file containing the delayerSettings has been added.
    $this->assertArrayNotHasKey('delayerSettings', $inputJavascript);
    $this->assertArrayHasKey('delayerSettings', $resultJavascript);

    // Check that there are the right number of files generated.
    // 1 for delayerSettings.
    // 1 for each aggregated group.
    // 0 for aggregated group that are not matching anything or all external.
    // In our test, we have group 1 and 3.
    $array_files = $this->fileSystem->scanDirectory('public://js/delayer', '/.*/');
    $this->assertCount(3, $array_files);

    // Check content of aggregated files.
    // The name of the file depends on the content.
    $jsGroup1 = Crypt::hashBase64("console.log('js2');;\nconsole.log('js8');;\n");
    $this->assertFileExists('public://js/delayer/delay_' . $jsGroup1 . '.js');

    $jsGroup2 = Crypt::hashBase64("console.log('js9');;\n");
    $this->assertFileExists('public://js/delayer/delay_' . $jsGroup2 . '.js');

    // Check content of file jsDelayerSettings.
    // Expecting something like:
    /*
    var jsDelayerSettings = {"mobile":false,"mobileWidth":null};var jsDelayerGlobalStrategy = [{"js":[{"type":"external","data":"http:\/\/localhost\/vfs:\/\/root\/sites\/simpletest\/70839939\/files\/js\/delayer\/delay_Dg1UqurxcAobACO8HOODLzurmmxid_QezTdHWYoZqNo.js"}],"timing":100,"sync":1},{"js":[{"data":"modules\/custom\/libraries_delay_load\/tests\/modules\/libraries_delay_load_javascript_test\/js\/js1.js","type":"file"},{"data":"modules\/custom\/libraries_delay_load\/tests\/modules\/libraries_delay_load_javascript_test\/js\/js5.js","type":"file"}],"timing":500,"sync":0},{"js":[{"type":"external","data":"http:\/\/localhost\/vfs:\/\/root\/sites\/simpletest\/70839939\/files\/js\/delayer\/delay_AmTY515-snnKtgHUxZCaCW7WsQWPZ2pIeZfumhjpzeg.js"}],"timing":1000,"sync":0},{"js":[],"timing":1000,"sync":1},{"js":[{"data":"https:\/\/load.external.com\/test1.js","type":"external"}],"timing":1000,"sync":1}];
     */
    $fileDelayerSettingsContent = file_get_contents($resultJavascript['delayerSettings']['data']);

    // Check that it contains the group with the righ timing and sync settings.
    $this->assertStringContainsString($jsGroup1 . '.js"}],"timing":100,"sync":1}', $fileDelayerSettingsContent);
    $this->assertStringContainsString($jsGroup2 . '.js"}],"timing":1000,"sync":0}', $fileDelayerSettingsContent);

    // Check that it's loading the other js as defined in the config.
    $this->assertStringContainsString('js1.js', $fileDelayerSettingsContent);
    $this->assertStringContainsString('js5.js', $fileDelayerSettingsContent);
    $this->assertStringContainsString('load.external.com\/test1.js', $fileDelayerSettingsContent);

    // And that it doesn't contain other stuff.
    $this->assertStringNotContainsString('js3.js', $fileDelayerSettingsContent);
    $this->assertStringNotContainsString('non_existing_js1.js', $fileDelayerSettingsContent);
    $this->assertStringNotContainsString('non_existing_js2.js', $fileDelayerSettingsContent);
    $this->assertStringNotContainsString('non_existing_complex_js.js', $fileDelayerSettingsContent);

    // Check that the mobile strategy is properly not applied.
    $this->assertStringContainsString('"mobile":false', $fileDelayerSettingsContent);
    $this->assertStringContainsString('jsDelayerGlobalStrategy', $fileDelayerSettingsContent);
    $this->assertStringNotContainsString('jsDelayerMobileStrategy', $fileDelayerSettingsContent);
  }

  /**
   * Test the Mobile settings are properly handled.
   */
  public function testMobile() {
    $this->setupStrategiesMobile();

    $inputJavascript = $this->getInputJavascript();

    $resultJavascript = $inputJavascript;
    $this->jsDelayer->processAssetArray($resultJavascript);

    // Testing that excluded / delayed js are properly removed from the list.
    // Default strategy 1 should be applied.
    // From excluded.
    $this->assertArrayHasKey($this->jsPath . 'js1.js', $inputJavascript);
    $this->assertArrayNotHasKey($this->jsPath . 'js1.js', $resultJavascript);

    // And from various groups.
    $this->assertArrayHasKey($this->jsPath . 'js8.js', $inputJavascript);
    $this->assertArrayNotHasKey($this->jsPath . 'js8.js', $resultJavascript);
    $this->assertArrayHasKey($this->jsPath . 'js5.js', $inputJavascript);
    $this->assertArrayNotHasKey($this->jsPath . 'js5.js', $resultJavascript);

    // Check that some values are still kept.
    $randomJs = $this->jsPath . 'js_random.js';
    $this->assertArrayHasKey($randomJs, $resultJavascript);
    $this->assertArrayHasKey($this->jsPath . 'js6.js', $resultJavascript);
    $selfJs = drupal_get_path('module', 'libraries_delay_load') . '/js/delayer.js';
    $this->assertArrayHasKey($selfJs, $resultJavascript);

    // Check that a file containing the delayerSettings has been added.
    $this->assertArrayNotHasKey('delayerSettings', $inputJavascript);
    $this->assertArrayHasKey('delayerSettings', $resultJavascript);

    // Check that there are the right number of files generated.
    // 1 for delayerSettings.
    // 1 for each aggregated group.
    // 0 for aggregated group that are not matching anything or all external.
    // In our test, we have group 1 and 3 for global.
    // and group 1, group 2 and group 5 for mobile
    // but js9.js is common for both groups
    // so we expect 1 + 2 + 3 - 1 = 5 files.
    $array_files = $this->fileSystem->scanDirectory('public://js/delayer', '/.*/');
    $this->assertCount(5, $array_files);

    // Check content of aggregated files.
    // The name of the file depends on the content.
    $jsGroup1 = Crypt::hashBase64("console.log('js2');;\nconsole.log('js8');;\n");
    $this->assertFileExists('public://js/delayer/delay_' . $jsGroup1 . '.js');

    $jsGroup2 = Crypt::hashBase64("console.log('js9');;\n");
    $this->assertFileExists('public://js/delayer/delay_' . $jsGroup2 . '.js');

    // Check content of file jsDelayerSettings.
    // Expecting something like:
    /*
    var jsDelayerSettings = {"mobile":true,"mobileWidth":600};var jsDelayerGlobalStrategy = [{"js":[{"type":"external","data":"http:\/\/localhost\/vfs:\/\/root\/sites\/simpletest\/29430312\/files\/js\/delayer\/delay_Dg1UqurxcAobACO8HOODLzurmmxid_QezTdHWYoZqNo.js"}],"timing":100,"sync":1},{"js":[{"data":"modules\/custom\/libraries_delay_load\/tests\/modules\/libraries_delay_load_javascript_test\/js\/js1.js","type":"file"},{"data":"modules\/custom\/libraries_delay_load\/tests\/modules\/libraries_delay_load_javascript_test\/js\/js5.js","type":"file"}],"timing":500,"sync":0},{"js":[{"type":"external","data":"http:\/\/localhost\/vfs:\/\/root\/sites\/simpletest\/29430312\/files\/js\/delayer\/delay_AmTY515-snnKtgHUxZCaCW7WsQWPZ2pIeZfumhjpzeg.js"}],"timing":1000,"sync":0},{"js":[],"timing":1000,"sync":1},{"js":[{"data":"https:\/\/load.external.com\/test1.js","type":"external"}],"timing":1000,"sync":1}];var jsDelayerMobileStrategy = [{"js":[{"type":"external","data":"http:\/\/localhost\/vfs:\/\/root\/sites\/simpletest\/29430312\/files\/js\/delayer\/delay_S45vQ9fIZsgHpZ-tw90vrZ0xCHzA_rAsEOb_EsSX9g4.js"}],"timing":1000,"sync":1},{"js":[{"type":"external","data":"http:\/\/localhost\/vfs:\/\/root\/sites\/simpletest\/29430312\/files\/js\/delayer\/delay_K6MMrLlrM2bSxqBoMx3YLJjJ_rDVu9mnRBylVIyJg5M.js"}],"timing":1000,"sync":1},{"js":[{"data":"modules\/custom\/libraries_delay_load\/tests\/modules\/libraries_delay_load_javascript_test\/js\/js1.js","type":"file"}],"timing":500,"sync":0},{"js":[{"data":"modules\/custom\/libraries_delay_load\/tests\/modules\/libraries_delay_load_javascript_test\/js\/js5.js","type":"file"}],"timing":500,"sync":0},{"js":[{"type":"external","data":"http:\/\/localhost\/vfs:\/\/root\/sites\/simpletest\/29430312\/files\/js\/delayer\/delay_AmTY515-snnKtgHUxZCaCW7WsQWPZ2pIeZfumhjpzeg.js"}],"timing":1000,"sync":0},{"js":[],"timing":1000,"sync":1},{"js":[{"data":"https:\/\/load.external.com\/test1.js","type":"external"}],"timing":1000,"sync":1}];
     */
    $fileDelayerSettingsContent = file_get_contents($resultJavascript['delayerSettings']['data']);

    // Check that it contains the group with the righ timing and sync settings.
    $this->assertStringContainsString($jsGroup1 . '.js"}],"timing":100,"sync":1}', $fileDelayerSettingsContent);
    $this->assertStringContainsString($jsGroup2 . '.js"}],"timing":1000,"sync":0}', $fileDelayerSettingsContent);

    // Check that it's loading the other js as defined in the config.
    $this->assertStringContainsString('js1.js', $fileDelayerSettingsContent);
    $this->assertStringContainsString('js5.js', $fileDelayerSettingsContent);
    $this->assertStringContainsString('load.external.com\/test1.js', $fileDelayerSettingsContent);

    // And that it doesn't contain other stuff.
    $this->assertStringNotContainsString('js3.js', $fileDelayerSettingsContent);
    $this->assertStringNotContainsString('non_existing_js1.js', $fileDelayerSettingsContent);
    $this->assertStringNotContainsString('non_existing_js2.js', $fileDelayerSettingsContent);
    $this->assertStringNotContainsString('non_existing_complex_js.js', $fileDelayerSettingsContent);

    // Check that the mobile strategy is properly applied.
    $this->assertStringContainsString('"mobile":true', $fileDelayerSettingsContent);
    $this->assertStringContainsString('"mobileWidth":600', $fileDelayerSettingsContent);
    $this->assertStringContainsString('jsDelayerGlobalStrategy', $fileDelayerSettingsContent);
    $this->assertStringContainsString('jsDelayerMobileStrategy', $fileDelayerSettingsContent);
  }

  /**
   * Test the strategy selection.
   */
  public function testStrategySelection() {
    $this->setupStrategiesConfig2();

    $private_method = $this
      ->getAccessibleMethod('Drupal\\libraries_delay_load\\Asset\\JsDelayer', 'getStrategy');

    // Use the reflection to invoke on the object.
    $strategy = $private_method->invokeArgs($this->jsDelayer, []);

    // It should take the one with the least weight.
    $this->assertTrue($strategy->id() === 'test_strategy_2');
  }

  /**
   * Test empty strategy.
   */
  public function testEmptyStrategy() {
    $inputJavascript = $this->getInputJavascript();

    $resultJavascript = $inputJavascript;
    $this->jsDelayer->processAssetArray($resultJavascript);

    // Remove delayer.js, as should have been removed by the function.
    $delayerJsFile = drupal_get_path('module', 'libraries_delay_load') . '/js/delayer.js';
    unset($inputJavascript[$delayerJsFile]);

    $this->assertEquals($resultJavascript, $inputJavascript);
  }

  /**
   * Test what happens when the module is disabled.
   */
  public function testIsDisabled() {
    $this->settingsEnable(FALSE);
    $isEnabled = $this->jsDelayer->isEnabled();
    $this->assertFalse($isEnabled);
  }

  /**
   * Get an accessible method using reflection.
   */
  private function getAccessibleMethod($class_name, $method_name) {
    $class = new \ReflectionClass($class_name);
    $method = $class
      ->getMethod($method_name);
    $method
      ->setAccessible(TRUE);
    return $method;
  }

  /**
   * Initialize the javascripts.
   */
  private function getInputJavascript() {
    // Initializing the javascript array.
    $inputJavascript = [];
    for ($i = 0; $i < 10; $i++) {
      $value = $this->jsPath . 'js' . $i . '.js';
      // Faking the javascript array.
      $inputJavascript[$value] = ['type' => 'file', 'data' => $value];
    }
    // Adding a random value.
    $randomJs = $this->jsPath . 'js_random.js';
    $inputJavascript[$randomJs] = ['type' => 'file', 'data' => $randomJs];

    // Adding external javascripts.
    $externalJs1 = 'https://load.external.com/test1.js';
    $inputJavascript[$externalJs1] = ['type' => 'external', 'data' => $externalJs1];
    $externalJs2 = 'https://load.external.com/test2.js';
    $inputJavascript[$externalJs2] = ['type' => 'external', 'data' => $externalJs2];

    // We need to add our javascript.
    $selfJs = drupal_get_path('module', 'libraries_delay_load') . '/js/delayer.js';
    $inputJavascript[$selfJs] = ['type' => 'file', 'data' => $selfJs];

    return $inputJavascript;
  }

  /**
   * Enable / Disable the module in the settings.
   */
  protected function settingsEnable($bool) {
    $config = $this->container->get('config.factory')->getEditable('libraries_delay_load.admin_settings');
    $config
      ->set('enabled', $bool)
      ->save();
  }

  /**
   * Enable / Disable the debug mode in the settings.
   */
  protected function settingsDebug($bool) {
    $config = $this->container->get('config.factory')->getEditable('libraries_delay_load.admin_settings');
    $config
      ->set('debug', $bool)
      ->save();
  }

  /**
   * Create the strategy configuration #1.
   */
  protected function setupStrategiesConfig1() {
    // Set-up 1 complex group with external & non existing files.
    $complexGroup = $this->setupStrategiesConfigGroup(1000, 1, TRUE, [
      'non_existing_complex_js.js',
    ]);
    $complexGroup['javascript'] .= "\r\nhttps://load.external.com/test1.js";

    $config_entity = $this->jsDelayLoadStorage->create([
      'id' => 'test_strategy_1',
      'excluded' => $this->implodeMultiLine([
        'js3.js',
        'js4.js',
        'js7.js',
        'non_existing_js1.js',
      ]),
      'mobile' => 0,
      'weight' => -5,
      'groups' => [
        $this->setupStrategiesConfigGroup(100, 1, TRUE, [
          'js2.js',
          'js8.js',
          'non_existing_js2.js',
        ]),
        $this->setupStrategiesConfigGroup(500, 0, FALSE, [
          'js1.js',
          'js5.js',
        ]),
        $this->setupStrategiesConfigGroup(1000, 0, TRUE, [
          'js9.js',
        ]),
        $this->setupStrategiesConfigGroup(1000, 1, FALSE, [
          'non_existing_js3.js',
          'non_existing_js4.js',
        ]),
        $complexGroup,
      ],
      'mobile_groups' => [],
    ]);
    $config_entity->save();
  }

  /**
   * Create the mobile strategy configuration.
   */
  protected function setupStrategiesMobile() {
    // Set-up 1 complex group with external & non existing files.
    $complexGroup = $this->setupStrategiesConfigGroup(1000, 1, TRUE, [
      'non_existing_complex_js.js',
    ]);
    $complexGroup['javascript'] .= "\r\nhttps://load.external.com/test1.js";

    $config_entity = $this->jsDelayLoadStorage->create([
      'id' => 'test_strategy_1',
      'excluded' => $this->implodeMultiLine([
        'js3.js',
        'js4.js',
        'js7.js',
        'non_existing_js1.js',
      ]),
      'mobile' => 1,
      'mobile_width' => 600,
      'weight' => -5,
      'groups' => [
        $this->setupStrategiesConfigGroup(100, 1, TRUE, [
          'js2.js',
          'js8.js',
          'non_existing_js2.js',
        ]),
        $this->setupStrategiesConfigGroup(500, 0, FALSE, [
          'js1.js',
          'js5.js',
        ]),
        $this->setupStrategiesConfigGroup(1000, 0, TRUE, [
          'js9.js',
        ]),
        $this->setupStrategiesConfigGroup(1000, 1, FALSE, [
          'non_existing_js3.js',
          'non_existing_js4.js',
        ]),
        $complexGroup,
      ],
      /* Same files n a different order & different settings*/
      'mobile_groups' => [
        $this->setupStrategiesConfigGroup(1000, 1, TRUE, [
          'js2.js',
        ]),
        $this->setupStrategiesConfigGroup(1000, 1, TRUE, [
          'js8.js',
          'non_existing_js2.js',
        ]),
        $this->setupStrategiesConfigGroup(500, 0, FALSE, [
          'js1.js',
        ]),
        $this->setupStrategiesConfigGroup(500, 0, FALSE, [
          'js5.js',
          'non_existing_js4.js',
        ]),
        $this->setupStrategiesConfigGroup(1000, 0, TRUE, [
          'js9.js',
        ]),
        $this->setupStrategiesConfigGroup(1000, 1, FALSE, [
          'non_existing_js3.js',
        ]),
        $complexGroup,
      ],
    ]);
    $config_entity->save();
  }

  /**
   * Create the strategy configuration #2.
   */
  protected function setupStrategiesConfig2() {
    $config_entity = $this->jsDelayLoadStorage->create([
      'id' => 'test_strategy_1',
      'excluded' => '',
      'weight' => 2,
      'groups' => [],
      'mobile' => 0,
      'mobile_groups' => [],
    ]);
    $config_entity->save();

    $config_entity = $this->jsDelayLoadStorage->create([
      'id' => 'test_strategy_2',
      'excluded' => '',
      'weight' => 1,
      'groups' => [],
      'mobile' => 0,
      'mobile_groups' => [],
    ]);
    $config_entity->save();

    $config_entity = $this->jsDelayLoadStorage->create([
      'id' => 'test_strategy_3',
      'excluded' => '',
      'weight' => 3,
      'groups' => [],
      'mobile' => 0,
      'mobile_groups' => [],
    ]);
    $config_entity->save();
  }

  /**
   * Create a strategy group.
   */
  protected function setupStrategiesConfigGroup($timing, $synchro, $aggregate, array $javascriptArray) {
    return [
      'timing' => $timing,
      'synchro' => $synchro,
      'aggregate' => $aggregate,
      'javascript' => $this->implodeMultiLine($javascriptArray),
    ];
  }

  /**
   * Implode an array into a multi-line text.
   */
  protected function implodeMultiLine($array) {
    $return = [];
    foreach ($array as $javascript) {
      $return[] = $this->jsPath . $javascript;
    }

    return implode($return, "\r\n");
  }

}
