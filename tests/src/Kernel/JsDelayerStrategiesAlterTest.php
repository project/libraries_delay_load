<?php

namespace Drupal\Tests\libraries_delay_load\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the JsDelayer service focusing on the strategies.
 *
 * @group libraries_delay_load
 */
class JsDelayerStrategiesAlterTest extends KernelTestBase {

  /**
   * The service under test.
   *
   * @var \Drupal\libraries_delay_load\JsDelayer
   */
  protected $jsDelayer;

  /**
   * The entity storage for JS Delay Load config entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $jsDelayLoadStorage;

  /**
   * The path in front of the js/.
   *
   * @var string
   */
  protected $jsPath;

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  public static $modules = [
    'libraries_delay_load', 'libraries_delay_load_strategies_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installConfig(['libraries_delay_load']);

    $this->jsDelayer = \Drupal::service('libraries_delay_load.delayer.js');

    $this->jsDelayLoadStorage = $this->container->get('entity_type.manager')->getStorage('js_delay_strategy');

    $this->jsPath = '/path/to/';

    $this->setupStrategiesConfig();
  }

  /**
   * Enable / Disable the module in the settings.
   */
  protected function settingsEnable($bool) {
    $config = $this->container->get('config.factory')->getEditable('libraries_delay_load.admin_settings');
    $config
      ->set('enabled', $bool)
      ->save();
  }

  /**
   * Enable / Disable the debug mode in the settings.
   */
  protected function settingsDebug($bool) {
    $config = $this->container->get('config.factory')->getEditable('libraries_delay_load.admin_settings');
    $config
      ->set('debug', $bool)
      ->save();
  }

  /**
   * Create the default stratgies configuration.
   */
  protected function setupStrategiesConfig() {
    $config_entity = $this->jsDelayLoadStorage->create([
      'id' => 'test_strategy_1',
      'excluded' => '',
      'weight' => -5,
      'groups' => [],
      'mobile' => 0,
      'mobile_groups' => [],
    ]);
    $config_entity->save();

    $config_entity = $this->jsDelayLoadStorage->create([
      'id' => 'test_strategy_2',
      'excluded' => '',
      'weight' => 0,
      'groups' => [],
      'mobile' => 0,
      'mobile_groups' => [],
    ]);
    $config_entity->save();

    $config_entity = $this->jsDelayLoadStorage->create([
      'id' => 'test_strategy_3',
      'excluded' => '',
      'weight' => 5,
      'groups' => [],
      'mobile' => 0,
      'mobile_groups' => [],
    ]);
    $config_entity->save();
  }

  /**
   * Test the strategy selection with alter subscriber.
   */
  public function testStrategySelection() {

    $private_method = $this
      ->getAccessibleMethod('Drupal\\libraries_delay_load\\Asset\\JsDelayer', 'getStrategy');

    // Use the reflection to invoke on the object.
    $strategy = $private_method->invokeArgs($this->jsDelayer, []);

    // Should be test_strategy_3 because of event subscriber.
    $this->assertTrue($strategy->id() === 'test_strategy_3');
  }

  /**
   * Get an accessible method using reflection.
   */
  private function getAccessibleMethod($class_name, $method_name) {
    $class = new \ReflectionClass($class_name);
    $method = $class
      ->getMethod($method_name);
    $method
      ->setAccessible(TRUE);
    return $method;
  }

}
