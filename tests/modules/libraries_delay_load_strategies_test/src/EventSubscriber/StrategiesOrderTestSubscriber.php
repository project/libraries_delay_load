<?php

namespace Drupal\libraries_delay_load_strategies_test\EventSubscriber;

use Drupal\libraries_delay_load\Event\StrategiesOrderAlter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class StrategiesOrderTestSubscriber.
 *
 * @package Drupal\libraries_delay_load_strategies_test\StrategiesOrderTestSubscriber
 */
class StrategiesOrderTestSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      StrategiesOrderAlter::EVENT_NAME => 'updateOrder',
    ];
  }

  /**
   * Set the last strategy as the one to use.
   *
   * @param \Drupal\libraries_delay_load\Event\StrategiesOrderAlter $event
   *   The event.
   */
  public function updateOrder(StrategiesOrderAlter $event) {
    // Retrieve strategies.
    $strategies = $event->getStrategies();
    $strategy = array_pop($strategies);

    $event->setStrategies([$strategy]);
  }

}
