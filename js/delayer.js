(function () {
  'use strict';

  var isMobile = false;

  // Wait for jsDelayerSettings, drupalSettings & jQuery to be available as most scripts depend on it.
  window.addEventListener('load', function () {
    waitForSettings();
  });

  function waitForSettings() {
    if (typeof jsDelayerSettings !== 'undefined' && typeof drupalSettings !== 'undefined' && typeof jQuery !== 'undefined') {
      // Should we use the mobile strategy?
      if (jsDelayerSettings['mobile'] === true && jQuery(document).width() < jsDelayerSettings['mobileWidth']) {
        delayLogger('using mobile strategy');
        isMobile = true;
      }
      else {
        delayLogger('using global strategy');
      }
      // Prepare the first group.
      prepareGroup(0);
    }
    else {
      delayLogger('waiting for jsDelayerSettings, drupalSettings & jQuery');
      setTimeout(waitForSettings, 200);
    }
  }

  // Start a given group after the configured delay.
  function prepareGroup(index) {
    let groups;
    if (isMobile) {
      groups = jsDelayerMobileStrategy;
    }
    else {
      groups = jsDelayerGlobalStrategy;
    }

    // If we haven't reached the last group.
    if (typeof groups[index] !== 'undefined') {
      let group;
      group = groups[index];
      // 1 script after the other.
      if (parseInt(group['sync']) === 1) {
        delayLogger('Starting sync group ' + (index + 1) + ' in ' + group['timing'] + 'ms');
        setTimeout(loadSyncGroup, group['timing'], group, index, 0);
      }
      // Async Loading.
      else {
        delayLogger('Starting async group ' + (index + 1) + ' in ' + group['timing'] + 'ms');
        setTimeout(loadAsyncGroup, group['timing'], group, index);
      }
    }
    else {
      delayLogger('js delayer is done');
    }
  }

  // Load scripts in a group, one after the other.
  function loadSyncGroup(group, groupIndex, scriptIndex) {
    if (typeof group.js[scriptIndex] !== 'undefined') {
      let script = group.js[scriptIndex];
      let url = script.data;
      if (script.type === 'file') {
        url = drupalSettings.path.baseUrl + url;
      }
      delayLogger('Starting to call sync ' + url);
      jQuery.getScript(url).done(function () {
        delayLogger('Done calling sync ' + url);
        loadSyncGroup(group, groupIndex, scriptIndex + 1);
      });
    }
    else {
      // Attach behaviors after everything is loaded.
      delayLogger('everything loaded sync group ' + (groupIndex + 1));
      Drupal.attachBehaviors();
      prepareGroup(groupIndex + 1);
    }
  }

  // Load scripts in a group in parallel
  function loadAsyncGroup(group, groupIndex) {
    for (let scriptIndex = 0; scriptIndex < group.js.length; scriptIndex++) {
      let script = group.js[scriptIndex];
      let url = script.data;
      if (script.type === 'file') {
        url = drupalSettings.path.baseUrl + url;
      }
      delayLogger('Starting to call async ' + url);
      let s = document.createElement('script');
      s.type = 'text/javascript';
      s.async = false;
      s.src = url;
      let x = document.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);
    }
    // No point in running behaviors as scripts probably won't have been downloaded yet
    delayLogger('everything called async group ' + (groupIndex + 1));
    prepareGroup(groupIndex + 1);
  }

  function delayLogger(text) {
    if (typeof drupalSettings !== 'undefined' && typeof drupalSettings.librariesDelayLoadLogger !== 'undefined') {
      console.log(Date.now() + ' - ' + text);
    }
  }

})();
