<?php

namespace Drupal\libraries_delay_load_rules_integration\Plugin\Condition;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\rules\Core\RulesConditionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Current route' condition.
 *
 * Greatly inspired by the route_condition module.
 *
 * @Condition(
 *   id = "libraries_delay_load_route_condition",
 *   label = @Translation("Current route"),
 *   category = @Translation("System"),
 *   context_definitions = {
 *     "routes" = @ContextDefinition("string",
 *       label = @Translation("Routes"),
 *       description = @Translation("Condition will validate if the page requested is any of the routes. The '*' character is a wildcard"),
 *       multiple = TRUE,
 *       assignment_restriction = "input"
 *     )
 *   }
 * )
 */
class RouteCondition extends RulesConditionBase implements ContainerFactoryPluginInterface {

  /**
   * The CurrentRouteMatch service.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $currentRouteMatch;

  /**
   * Constructs an EntityHasField object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $current_route_match
   *   The CurrentRouteMatch service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, CurrentRouteMatch $current_route_match) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentRouteMatch = $current_route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_route_match')
    );
  }

  /**
   * Evaluate if user has role(s).
   *
   * @param array $routes
   *   The list of routes to check.
   *
   * @return bool
   *   TRUE if the page requested is any of the routes
   */
  protected function doEvaluate(array $routes) {
    $current_route_name = $this->currentRouteMatch->getRouteName();

    foreach ($routes as $route) {
      if ($route === $current_route_name || $this->evaluateRouteWildcards($route, $current_route_name)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Evaluate wildcards in route patterns.
   *
   * @param string $route_pattern
   *   The route to evaluate for wildcards.
   * @param string $current_route_name
   *   The current request route name.
   *
   * @return bool
   *   Indication whether the provided route pattern matches the current route.
   */
  protected function evaluateRouteWildcards($route_pattern, $current_route_name) {
    if (strpos($route_pattern, '*') === FALSE) {
      return FALSE;
    }
    $escaped_route_pattern = str_replace('.', '\.', $route_pattern);
    $route_pattern_wildcards = str_replace('*', '.*', $escaped_route_pattern);
    $regex = "{^{$route_pattern_wildcards}$}";

    return (bool) preg_match($regex, $current_route_name);
  }

}
