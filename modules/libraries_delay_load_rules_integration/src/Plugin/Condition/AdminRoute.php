<?php

namespace Drupal\libraries_delay_load_rules_integration\Plugin\Condition;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\rules\Core\RulesConditionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an 'Is admin route' condition.
 *
 * @Condition(
 *   id = "libraries_delay_load_admin_route",
 *   label = @Translation("Is admin route"),
 *   category = @Translation("System")
 * )
 */
class AdminRoute extends RulesConditionBase implements ContainerFactoryPluginInterface {

  /**
   * The AdminContext service.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Constructs an EntityHasField object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The AdminContext service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AdminContext $admin_context) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->adminContext = $admin_context;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('router.admin_context')
    );
  }

  /**
   * Evaluate if the route is an admin route.
   *
   * @return bool
   *   TRUE if the page requested is an admin route.
   */
  protected function doEvaluate() {
    return $this->adminContext->isAdminRoute();
  }

}
