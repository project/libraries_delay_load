<?php

namespace Drupal\libraries_delay_load_rules_integration\Plugin\RulesAction;

use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\rules\Core\RulesActionBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Set Js Strategy' action.
 *
 * @RulesAction(
 *   id = "libraries_delay_load_set_strategy",
 *   label = @Translation("Set a strategy for this page (if exsting)"),
 *   category = @Translation("JS Delay Load"),
 *   context_definitions = {
 *     "strategies" = @ContextDefinition("string",
 *       label = @Translation("Ordered list of strategies machine names"),
 *       multiple = TRUE,
 *       assignment_restriction = "selector"
 *     ),
 *     "strategyId" = @ContextDefinition("string",
 *       label = @Translation("The strategy to push first"),
 *       assignment_restriction = "input",
 *       list_options_callback = "strategiesListOptions"
 *     )
 *   }
 * )
 */
class SetJsStrategy extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * The js delay library storage.
   *
   * @var \Drupal\Core\Entity\ConfigEntityStorage
   */
  protected $jsDelayLibraryStorage;

  /**
   * Constructs an EntityHasField object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\ConfigEntityStorage $js_delay_library_storage
   *   The js delay library storage.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigEntityStorage $js_delay_library_storage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->jsDelayLibraryStorage = $js_delay_library_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')->getStorage('js_delay_strategy')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array $strategies, $strategyId) {
    // Setting strategyId as the first element of the array if it exists.
    $key = array_search($strategyId, $strategies);
    if ($key) {
      unset($strategies[$key]);
      array_unshift($strategies, $strategyId);
    }
    $this->getContext('strategies')->getContextData()->setValue($strategies);
  }

  /**
   * Returns an array of existing strategies.
   *
   * @return array
   *   An array of strategies in no particular order.
   */
  public function strategiesListOptions() {
    $strategies = $this->jsDelayLibraryStorage->getQuery()->execute();
    return $strategies;
  }

}
