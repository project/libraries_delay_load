<?php

namespace Drupal\libraries_delay_load_rules_integration\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;

/**
 * Provides a 'Remove all js strategies' action.
 *
 * @RulesAction(
 *   id = "libraries_delay_load_remove_strategies",
 *   label = @Translation("Remove JS Strategies for this page"),
 *   category = @Translation("JS Delay Load"),
 *   context_definitions = {
 *     "strategies" = @ContextDefinition("string",
 *       label = @Translation("Ordered list of strategies machine names"),
 *       multiple = TRUE,
 *       assignment_restriction = "selector",
 *     )
 *   }
 * )
 */
class RemoveAllJsStrategies extends RulesActionBase {

  /**
   * {@inheritdoc}
   */
  protected function doExecute(array $strategies) {
    $this->getContext('strategies')->getContextData()->setValue([]);
  }

}
