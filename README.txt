INTRODUCTION
------------

A D8 / D9 module focused on javascript loading improvements

Traditionally, all the javascript of a page is loaded during page load.
Best case scenario, it's loaded using defer (so towards the end).
And using async is usually a mess as it does not manage dependencies
between scripts.

This modules allows to see the javascripts that are loaded on a page.
You can then decide to remove them if they are not useful.
Or you can decide to load them at a later stage if they are not on the
critical path of rendering.

Doing this lightens the load on the browser and allows to serve useful
content to the user faster.

This will in turn improve your lighthouse / page speed insights analytics
which is a very important matter for SEO (better ranking with google).

As an example, without optimizing any javascripts, on a site I am developing,
I went from 57 to 79 in page speed insights for mobile with only a very light
configuration.

This module is more towards developers as you need to understand the scripts
that are loaded and their dependencies to avoid breaking the display (or you
can do tests and see how it behaves).

Helpfully, the debug mode helps you see which scripts are being loaded on a
page and the order of the download / execution of scripts through this module.

You can specify multiple strategies - and then define which one to apply based
on conditions that you can either implement yourself subscribing to the
StrategiesOrderAlter::EVENT_NAME => 'updateOrder' event or use the Rules module
with the events & actions provided by this module.

Lastly, you can set-up a different configuration for mobile view versus Desktop
view, allowing you to get even better results on mobile speed testing.


Bonus: It plays well with advagg, as the hook to remove javascripts from
being loaded are ran right before advagg ones.

REQUIREMENTS
------------

* N/A


RECOMMENDED MODULES
-------------------

* Rules (https://www.drupal.org/project/rules) if you want to use Rules UI to
define applying a given strategy based on conditions.
To use Rules, you need to apply the patch in
https://www.drupal.org/project/rules/issues/3141206

INSTALLATION
------------

* Install as any Drupal D8 / D9 module
  
CONFIGURATION
------------

After the module is enabled, go to the module settings page at
admin/config/development/performance/libraries_delay_load/settings

There is a global settings page to turn on/off the library as well as the debug
mode.

On the List JS Delay Strategies tab, you can then define a 'strategy' which
consists of:
- a list of javascript that will never be loaded on the page

- indicating whether you want to have a different strategy configuration for
 mobile & for desktop, so that you can load hidden resources on mobile later.
Note: you need to have the same javascripts loaded later on desktop & mobile,
as applying the right strategy will be done only client side (meaning that 
javascript to remove from the regular processing are already removed)

- a chain of groups of javascript that will be removed from page load, and
loaded at a later stage through an ajax call. You specify the delay between the
window.load event and the first group as well as between groups.

Eg: Load -> 100ms -> Group 1 (important js) -> 300ms -> Group 2 (mid important
js) -> 1000ms -> Group 3 (less important js)

For each group, you can define if they should all be loaded in parallel
asynchronously (if you don't mind which one will be executed when) or in a
synchronized manner if there are dependencies between scripts.
Note: If you are loading scripts that are using behaviors, you should use
the synchronized way of loading so that attachBehaviors() is called

You can also define whether (local) scripts should be aggregated.

You can define multiple strategies: by default, the one at the top will be
executed but you can change this behavior. To do so, you can either:
- subscribe to the StrategiesOrderAlter::EVENT_NAME => 'updateOrder' which
sends a StrategiesOrderAlter $event in which you can get and set the list of
strategies' machine names. See
https://www.drupal.org/project/libraries_delay_load/issues/3137385 for an
example

- use Rules module, with the event 'Before choosing the JS Delay Strategy' and
actions 'Set a strategy for this page (if existing)' or 'Remove JS
Strategies for this page'. See an example and ask questions in
https://www.drupal.org/project/libraries_delay_load/issues/3139144
This module additionally provides 2 rules conditions to help you:
  - 'Is admin route'
  - 'Current route'


REAL LIFE EXAMPLES
------------
You can check <a href="https://www.discovereef.com/">
https://www.discovereef.com/</a> to see it in effect.
If you are using this module and want to showcase it, create a ticket in the
contact queue or contact me via message and I'll update this section
