<?php

namespace Drupal\libraries_delay_load\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Event that is fired after we retrieve the collection of available strategies.
 */
class StrategiesOrderAlter extends Event {

  const EVENT_NAME = 'libraries_delay_load_strategies_order_alter';

  /**
   * The ordered array of strategies id.
   *
   * Public because of https://www.drupal.org/project/rules/issues/2762517
   *
   * @var array
   */
  public $strategies;

  /**
   * Getter for the strategies array.
   *
   * @return array
   *   The strategies attribute.
   */
  public function getStrategies() {
    return $this->strategies;
  }

  /**
   * Setter for the strategies array.
   *
   * @param array $strategies
   *   The strategies array.
   */
  public function setStrategies(array $strategies) {
    $this->strategies = $strategies;
  }

  /**
   * Constructs the object.
   *
   * @param array $strategies
   *   The list of strategies loaded by entityQuery sorted by weight.
   */
  public function __construct(array $strategies) {
    $this->strategies = $strategies;
  }

}
