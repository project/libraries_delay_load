<?php

namespace Drupal\libraries_delay_load;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a JsDelayStrategy entity.
 */
interface JsDelayStrategyInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
