<?php

namespace Drupal\libraries_delay_load\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\libraries_delay_load\JsDelayStrategyInterface;

/**
 * Defines the JsDelayStrategy entity.
 *
 * @ConfigEntityType(
 *   id = "js_delay_strategy",
 *   label = @Translation("Libraries Delay Load"),
 *   handlers = {
 *     "list_builder" = "Drupal\libraries_delay_load\Controller\JsDelayStrategyListBuilder",
 *     "form" = {
 *       "add" = "Drupal\libraries_delay_load\Form\JsDelayStrategyForm",
 *       "edit" = "Drupal\libraries_delay_load\Form\JsDelayStrategyForm",
 *       "duplicate" = "Drupal\libraries_delay_load\Form\JsDelayStrategyDuplicateForm",
 *       "delete" = "Drupal\libraries_delay_load\Form\JsDelayStrategyDeleteForm",
 *     }
 *   },
 *   config_prefix = "js_delay_strategy",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "weight" = "weight",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "weight",
 *     "excluded",
 *     "mobile",
 *     "mobile_width",
 *     "groups",
 *     "mobile_groups",
 *   },
 *   links = {
 *     "edit-form" = "/admin/config/performance/libraries_delay_load/js_strategies/{libraries_delay_load}",
 *     "duplicate-form" = "/admin/config/performance/libraries_delay_load/js_strategies/{libraries_delay_load}/duplicate",
 *     "delete-form" = "/admin/config/performance/libraries_delay_load/js_strategies/{libraries_delay_load}/delete",
 *   }
 * )
 */
class JsDelayStrategy extends ConfigEntityBase implements JsDelayStrategyInterface {

  /**
   * The JsDelayStrategy ID.
   *
   * @var string
   */
  public $id;

  /**
   * The JsDelayStrategy label.
   *
   * @var string
   */
  public $label;

  /**
   * The JsDelayStrategy weight.
   *
   * @var int
   */
  public $weight;

  /**
   * The JsDelayStrategy mobile strategy boolean.
   *
   * @var bool
   */
  public $mobile;

  /**
   * The JsDelayStrategy mobile size parameter.
   *
   * @var int
   */
  public $mobile_width;

  /**
   * The JsDelayStrategy list of excluded javascripts.
   *
   * @var string
   */
  public $excluded;

  /**
   * The JsDelayStrategy groups of delayed javascripts.
   *
   * @var array
   */
  public $groups;

  /**
   * The JsDelayStrategy groups of delayed javascripts for mobile.
   *
   * @var array
   */
  public $mobile_groups;

}
