<?php

namespace Drupal\libraries_delay_load\Asset;

use Drupal\Component\Datetime\Time;
use Drupal\libraries_delay_load\Event\StrategiesOrderAlter;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Asset\AssetOptimizerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * The JavaScript Delayer.
 */
class JsDelayer {

  /**
   * A config object for the libraries_delay_load configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * A JS asset optimizer.
   *
   * @var \Drupal\Core\Asset\JsOptimizer
   */
  protected $optimizer;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * The js delay library storage.
   *
   * @var \Drupal\Core\Entity\ConfigEntityStorage
   */
  protected $jsDelayLibraryStorage;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * System time service.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * Constructs the JsDelayer object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   * @param \Drupal\Core\Asset\AssetOptimizerInterface $optimizer
   *   The optimizer for a single JS asset.
   * @param \Drupal\Core\Asset\FileSystemInterface $file_system
   *   The file system to write and delete files.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type sevice.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Component\Datetime\Time $time
   *    System time service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AssetOptimizerInterface $optimizer, FileSystemInterface $file_system, EntityTypeManagerInterface $entity_type_manager, EventDispatcherInterface $event_dispatcher, Time $time) {
    $this->config = $config_factory->get('libraries_delay_load.admin_settings');
    $this->optimizer = $optimizer;
    $this->fileSystem = $file_system;
    $this->jsDelayLibraryStorage = $entity_type_manager->getStorage('js_delay_strategy');
    $this->eventDispatcher = $event_dispatcher;
    $this->time = $time;
  }

  /**
   * Returns true if the module should run.
   */
  public function isEnabled() {
    return $this->config->get('enabled');
  }

  /**
   * Returns true if debug mode is enabled in the settings.
   */
  public function debugModeEnabled() {
    return $this->config->get('debug');
  }

  /**
   * Process a js asset array.
   *
   * @param array $javascript
   *   The js to process.
   */
  public function processAssetArray(array &$javascript) {

    // Retrieve the strategy to apply.
    $strategy = $this->getStrategy($javascript);

    if (!$strategy) {
      // Remove ourselves, as not useful on this page.
      $delayerJsFile = drupal_get_path('module', 'libraries_delay_load') . '/js/delayer.js';
      unset($javascript[$delayerJsFile]);
      return;
    }

    $this->debugMessage('JS delay load strategy applied on this page: ' . $strategy->get('label'));

    // Remove excluded javascripts as configured.
    $excluded = $strategy->get('excluded');
    $jsExcludedItems = explode("\r\n", $excluded);

    foreach ($jsExcludedItems as $jsId) {
      $jsId = trim($jsId);
      unset($javascript[$jsId]);
    }

    // Retrieve the javascript to load later through ajax.
    // We are guaranteed by the settings validation, that we have
    // the same list of javascripts on both groups & mobile groups.
    $mobileJsGroups = $this->getGroupStrategy('mobile_groups', $strategy, $javascript, FALSE);
    $globalJsGroups = $this->getGroupStrategy('groups', $strategy, $javascript, TRUE);

    // Create the settings file
    // Contains settings + configuration of files to load through ajax.
    $settings = [
      'mobile' => $strategy->get('mobile'),
      'mobileWidth' => $strategy->get('mobile_width'),
    ];
    $fileContent = 'var jsDelayerSettings = ' . json_encode($settings) . ';';
    $fileContent .= 'var jsDelayerGlobalStrategy = ' . json_encode($globalJsGroups) . ';';
    if ($strategy->get('mobile')) {
      $fileContent .= 'var jsDelayerMobileStrategy = ' . json_encode($mobileJsGroups) . ';';
    }

    $cid = Crypt::hashBase64($fileContent);
    $uri = $this->writeFile($fileContent, $cid);

    // Copy the js settings from libraries_delay_load.
    $delayerJsFile = drupal_get_path('module', 'libraries_delay_load') . '/js/delayer.js';
    $settingsFile = $javascript[$delayerJsFile];
    $settingsFile['data'] = $uri;
    $javascript['delayerSettings'] = $settingsFile;

    // Group with delayer to load them close to one another.
    $delayerJs = $javascript[$delayerJsFile];
    unset($javascript[$delayerJsFile]);
    $javascript[$delayerJsFile] = $delayerJs;

    // Debug info.
    $this->debugMessage('List of javascript still loaded directly on the page:');

    foreach ($javascript as $id => $script) {
      // Name and path could be different.
      $library = $id;
      if ($script['data'] != $library) {
        $library .= ' (' . $script['data'] . ')';
      }
      $this->debugMessage($library);
    }
  }

  /**
   * Return an array describing the strategy.
   *
   * And removes those javascript from the list if asked.
   */
  protected function getGroupStrategy($group, $strategy, &$javascript, $remove) {
    $jsDelayed = [];
    $jsGroups = $strategy->get($group);

    if ($jsGroups === NULL) {
      return $jsDelayed;
    }

    foreach ($jsGroups as $jsGroupId => $jsGroup) {
      $jsDelayed[$jsGroupId] = [];
      $jsGroupItems = explode("\r\n", $jsGroup['javascript']);

      $groupDelayed = [];
      foreach ($jsGroupItems as $jsId) {
        $jsId = trim($jsId);
        // Only loads the ones that are indeed required for this request.
        if (isset($javascript[$jsId])) {
          $item = [];
          $item['data'] = $jsId;
          $item['type'] = $javascript[$jsId]['type'];
          $groupDelayed[] = $item;
          if ($remove) {
            unset($javascript[$jsId]);
          }
        }
      }

      // Aggregates $jsDelayed if needed.
      if ($jsGroup['aggregate'] && !empty($groupDelayed)) {
        $this->aggregate($groupDelayed);
      }
      // Reorder keys as we may have removed some with unset.
      $groupDelayed = array_values($groupDelayed);

      $jsDelayed[$jsGroupId] = [
        'js' => $groupDelayed,
        'timing' => $jsGroup['timing'],
        'sync' => $jsGroup['synchro'],
      ];
    }
    return $jsDelayed;
  }

  /**
   * Find out the strategy to use.
   */
  protected function getStrategy() {
    // List all strategies, ordered by weight.
    $query = $this->jsDelayLibraryStorage->getQuery();
    $strategies = $query->sort('weight', 'ASC')->execute();

    // Allow other modules to modify it.
    $event = new StrategiesOrderAlter($strategies);

    // Get the event_dispatcher service and dispatch the event.
    $this->eventDispatcher->dispatch(StrategiesOrderAlter::EVENT_NAME, $event);

    // Get updated strategies list.
    $strategies = $event->getStrategies();

    // Function array_key_first has been introduced in PHP 7.3.
    // To remove when supporting only Drupal 9+.
    if (!function_exists('array_key_first')) {

      function array_key_first(array $arr) {
        foreach ($arr as $key => $unused) {
          return $key;
        }
        return NULL;
      }

    }

    // We take the first strategy.
    $strategyIdIndex = array_key_first($strategies);
    if ($strategyIdIndex === NULL) {
      $this->debugMessage('No JS delay load strategy on this page');
      return NULL;
    }

    $strategyId = $strategies[$strategyIdIndex];

    // Load the strategy.
    $strategy = $this->jsDelayLibraryStorage->load($strategyId);

    if (!$strategy) {
      $this->debugMessage('Strategy ' . $strategyId . ' could not be loaded');
      return NULL;
    }

    return $strategy;
  }

  /**
   * Concatenate the content of each file into one.
   *
   * Set this file's position at the first one to load
   * Note: it is not doing any minification.
   *
   * @param array $group
   *   The array of javascript.
   */
  protected function aggregate(array &$group) {
    $data = '';
    $position = NULL;
    foreach ($group as $index => $javascript) {
      // If it's a file, else we ignore.
      if ($javascript['type'] == 'file') {
        $javascript['preprocess'] = TRUE;
        $data .= $this->optimizer->optimize($javascript);
        // Append a ';' and a newline after each JS file to prevent them
        // from running together.
        $data .= ";\n";

        unset($group[$index]);

        // Saves the position if needed.
        if (!$position) {
          $position = $index;
        }
      }
    }

    if (!empty($data)) {
      $data = $this->optimizer->clean($data);

      $cid = Crypt::hashBase64($data);
      $uri = $this->writeFile($data, $cid);

      // Only workaround to get the URL from an URI with public://.
      $url = file_create_url($uri);

      // Sets the group at the right position.
      $group[$position] = ['type' => 'external', 'data' => $url];
      ksort($group);
    }
  }

  /**
   * The filename for the JS optimized file is the cid.
   *
   * Based on Asset Optimizer in advagg module.
   *
   * @param string $data
   *   The content to output.
   * @param string $cid
   *   The unique segment of the filename.
   *
   * @return bool|string
   *   FALSE or the saved filename.
   */
  protected function writeFile($data, $cid) {
    // Prefix filename to prevent blocking by firewalls which reject files
    // starting with "ad*".
    $path = 'public://js/delayer';
    $uri = $path . '/delay_' . $cid . '.js';

    // Create the JS file.
    if (empty($uri) || !file_exists($uri)) {
      $this->fileSystem->prepareDirectory($path, $this->fileSystem::CREATE_DIRECTORY);
      if (!file_exists($uri)) {
        if (!$this->fileSystem->saveData($data, $uri, $this->fileSystem::EXISTS_REPLACE)) {
          return FALSE;
        }
      }
    }
    return $uri;
  }

  /**
   * Delete all the generated files.
   */
  public function deleteFiles() {
    $path = 'public://js/delayer';
    $delete_stale = function ($uri) {
      // Default stale file threshold is 30 days.
      if ($this->time->getRequestTime() - filemtime($uri) > \Drupal::config('system.performance')->get('stale_file_threshold')) {
        $this->fileSystem->delete($uri);
      }
    };
    if (is_dir($path)) {
      $this->fileSystem->scanDirectory($path, '/.*/', ['callback' => $delete_stale]);
    }
  }

  /**
   * Display debug info if configured.
   */
  protected function debugMessage($message) {
    if ($this->config->get('debug')) {
      // Not injecting service on purpose, as debug mode shouldn't be activated
      // in prod.
      \Drupal::messenger()->addStatus($message);
    }
  }

}
