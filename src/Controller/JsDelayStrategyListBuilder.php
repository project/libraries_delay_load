<?php

namespace Drupal\libraries_delay_load\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\Entity\DraggableListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a listing of JsDelayStrategy configuration entities.
 *
 * @ingroup js_delay_strategy
 */
class JsDelayStrategyListBuilder extends DraggableListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'js_delay_strategy';
  }

  /**
   * Builds the header row for the entity listing.
   *
   * @return array
   *   A render array structure of header strings.
   *
   * @see \Drupal\Core\Entity\EntityListController::render()
   */
  public function buildHeader() {
    $header['label'] = $this->t('Label');
    return $header + parent::buildHeader();
  }

  /**
   * Builds a row for an entity in the entity listing.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity for which to build the row.
   *
   * @return array
   *   A render array of the table row for displaying the entity.
   *
   * @see \Drupal\Core\Entity\EntityListController::render()
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    return $row + parent::buildRow($entity);
  }

  /**
   * Adds some descriptive text to the strategy list.
   *
   * @return array
   *   Renderable array.
   */
  public function render() {

    $build['description'] = [
      '#markup' => $this->t("<p>By default, the first strategy in this table will be applied. This is why it is recommended to set a Default one as the first of the list.</p>
      <p>Then, other strategies could be prioritized using the Rules module or subscribing to the StrategiesOrderAlter::EVENT_NAME => 'updateOrder' event.</p>"),
    ];

    $build[] = parent::render();
    return $build;
  }

  /**
   * Invalidate necessary cache tags.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    Cache::invalidateTags(['library_info']);

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $entity */
    $operations = parent::getDefaultOperations($entity);

    if (!$entity->hasLinkTemplate('duplicate-form')) {
      return $operations;
    }
    $operations['duplicate'] = [
      'title' => $this->t('Duplicate'),
      'weight' => 20,
      'url' => $this->ensureDestination($entity->toUrl('duplicate-form')),
    ];

    return $operations;
  }

}
