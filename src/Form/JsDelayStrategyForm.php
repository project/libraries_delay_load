<?php

namespace Drupal\libraries_delay_load\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures forms module settings.
 */
class JsDelayStrategyForm extends EntityForm {

  /**
   * The entity storage factory for querying.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * Construct the JsDelayStrategyForm.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $entity_storage
   *   An entity query factory for the entity type.
   */
  public function __construct(EntityStorageInterface $entity_storage) {
    $this->entityStorage = $entity_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = new static($container->get('entity_type.manager')->getStorage('js_delay_strategy'));
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $form['#attached']['library'][] = 'libraries_delay_load/admin_settings';

    $config = $this->entity;

    // Build the form.
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $config->label(),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine name'),
      '#default_value' => $config->id(),
      '#machine_name' => [
        'exists' => [$this, 'exists'],
        'replace_pattern' => '([^a-z0-9_]+)',
        'error' => 'The machine-readable name must be unique, and can only contain lowercase letters, numbers, and underscores.',
      ],
      '#disabled' => !$config->isNew(),
    ];

    $form['strategies_title'] = [
      '#markup' => '<h1>Strategy</h1>',
    ];

    $form['excluded'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Select javascript to exclude'),
      '#default_value' => $config->get('excluded'),
      '#description' => $this->t('1 javascript path per line. eg: @example', ['@example' => 'core/assets/vendor/html5shiv/html5shiv.min.js']),
    ];

    $form['mobile'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Separate mobile & desktop strategies?'),
      '#default_value' => $config->get('mobile') ?? 0,
      '#description' => $this->t('If left unchecked, the same strategy will be applied to any device. If checked, you will be able to tailor the strategy for mobile versus desktop'),
    ];

    $form['mobile_width'] = [
      '#type' => 'number',
      '#required' => TRUE,
      '#title' => $this->t('Mobile size'),
      '#default_value' => $config->get('mobile_width') ?? 600,
      '#description' => $this->t('Mobile strategy will be applied for device with Width below the one configured here when javascript executes'),
      '#states' => [
        'visible' => [
          ':input[name="mobile"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['mobile_warning'] = [
      '#type' => 'item',
      '#title' => $this->t('<h5 class="warning">Warning: You need to have the same javascripts that are delayed in the Global strategy and in the mobile strategy</h5>'),
      '#description' => $this->t("But the number of groups, each group's setting and the javascripts per group could be different."),
      '#states' => [
        'visible' => [
          ':input[name="mobile"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $this->generateGroups($form, $form_state, $config, 'groups', 'nbGroups', 'group');
    $this->generateGroups($form, $form_state, $config, 'mobile_groups', 'nbMobileGroups', 'mobile group');

    return $form;
  }

  /**
   * Generate the form sections for groups.
   */
  protected function generateGroups(&$form, $form_state, $config, $dataName, $countName, $labelSingular) {

    $groups = $config->get($dataName);

    if (!isset($groups) || count($groups) === 0) {
      // At least 1 group.
      $configNbGroups = 1;
    }
    else {
      $configNbGroups = count($groups);
    }

    // If we added / removed groups through ajax, we should use those.
    $form_state->set($countName, $form_state->get($countName) ?? $configNbGroups);

    $title = $this->t('Global delay strategy');
    if ($dataName === 'mobile_groups') {
      $title = $this->t('Mobile delay strategy');
    }

    // Display mobile section only for mobile.
    $states = [];
    if ($dataName === 'mobile_groups') {
      $states = [
        'visible' => [
          ':input[name="mobile"]' => ['checked' => TRUE],
        ],
      ];
    }
    $form[$dataName] = [
      '#type' => 'fieldset',
      '#title' => $title,
      '#tree' => TRUE,
      '#attributes' => ['id' => $dataName . '_container'],
      '#states' => $states,
    ];

    for ($i = 0; $i < $form_state->get($countName); $i++) {
      $form[$dataName][$i] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@label @number', ['@label' => ucfirst($labelSingular), '@number' => $i + 1]),
        '#tree' => TRUE,
      ];

      if ($i == 0) {
        $title = $this->t('How long should this @label start to be loaded after the window load event is fired (in ms)', ['@label' => $labelSingular]);
      }
      else {
        $title = $this->t('How long should this @label start to be loaded after the load of the previous @label is completed (in ms)', ['@label' => $labelSingular]);
        $description = $this->t('If the previous @label is loaded asynchronously, scripts from this @label could start to be downloaded before previous ones are executed / finished', ['@label' => $labelSingular]);
      }
      $form[$dataName][$i]['timing'] = [
        '#type' => 'number',
        '#title' => $title,
        '#default_value' => $groups[$i]['timing'] ?? 200,
        '#description' => $description ?? '',
      ];
      $form[$dataName][$i]['synchro'] = [
        '#type' => 'radios',
        '#title' => $this->t('Scripts in that @label to be loaded one after the other (synchro) or at any time (asynchro)', ['@label' => $labelSingular]),
        '#default_value' => $groups[$i]['synchro'] ?? 1,
        '#options' => [
          0 => $this->t('Asynchro'),
          1 => $this->t('Synchro'),
        ],
        '#description' => $this->t('If choosing synchro, scripts will be fetched and executed in the order defined for the @label. Then all behaviors will be triggered. If choosing asynchro, scripts will be fetched in parallel and no behaviors will be triggered.', ['@label' => $labelSingular]),
      ];
      $form[$dataName][$i]['aggregate'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Aggregate all internal files in this @label into one?', ['@label' => $labelSingular]),
        '#default_value' => $groups[$i]['aggregate'] ?? NULL,
        '#description' => $this->t('Check this box if files need to be loaded together in order to have their behavior work properly. Within a group, aggregated files will be loaded at the position of the first file to aggregate.'),
      ];

      $form[$dataName][$i]['javascript'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Select javascript to delay, 1 javascript path per line'),
        '#default_value' => $groups[$i]['javascript'] ?? NULL,
        '#description' => $this->t('1 javascript path per line. eg: @example', ['@example' => 'core/assets/vendor/html5shiv/html5shiv.min.js']),
      ];
    }

    $functionName = 'groups';
    if ($dataName === 'mobile_groups') {
      $functionName = 'mobileGroups';
    }

    // Provide an "Add a new group" button.
    $form[$dataName]['add_' . $dataName] = [
      '#type' => 'submit',
      '#value' => $this->t('Add a new @label', ['@label' => $labelSingular]),
      '#submit' => [[$this, 'add' . ucfirst($functionName)]],
      '#limit_validation_errors' => [],
      '#ajax'   => [
        'callback' => [$this, $functionName . 'Callback'],
        'wrapper'  => $dataName . '_container',
      ],
    ];

    if ($form_state->get($countName) > 0) {
      // Provide a "Remove" button for latest selected group.
      $form[$dataName]['remove_' . $dataName] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove the last @label', ['@label' => $labelSingular]),
        '#submit' => [[$this, 'remove' . ucfirst($functionName)]],
        '#limit_validation_errors' => [],
        '#ajax'   => [
          'callback' => [$this, $functionName . 'Callback'],
          'wrapper'  => $dataName . '_container',
        ],
      ];
    }
  }

  /**
   * Checks for an existing LibrariesDelayForm config.
   *
   * @param string|int $entity_id
   *   The entity ID.
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return bool
   *   TRUE if this format already exists, FALSE otherwise.
   */
  public function exists($entity_id, array $element, FormStateInterface $form_state) {
    // Use the query factory to build a new entity query.
    $query = $this->entityStorage->getQuery();

    // Query the entity ID to see if its in use.
    $result = $query->condition('id', $element['#field_prefix'] . $entity_id)
      ->execute();

    // We don't need to return the ID, only if it exists or not.
    return (bool) $result;
  }

  /**
   * Implements callback for Ajax event on add/remove group.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   *   Groups section of the form.
   */
  public function groupsCallback(array &$form, FormStateInterface $form_state) {
    return $form['groups'];
  }

  /**
   * Implements callback for Ajax event on add/remove mobile group.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return array
   *   Groups section of the form.
   */
  public function mobileGroupsCallback(array &$form, FormStateInterface $form_state) {
    return $form['mobile_groups'];
  }

  /**
   * Adding a new group.
   */
  public function addGroups(array &$form, FormStateInterface $form_state) {
    $nbGroups = $form_state->get('nbGroups') + 1;
    $form_state->set('nbGroups', $nbGroups);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Removing the last group.
   */
  public function removeGroups(array &$form, FormStateInterface $form_state) {
    $nbGroups = $form_state->get('nbGroups') - 1;
    $form_state->set('nbGroups', $nbGroups);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Adding a new mobile group.
   */
  public function addMobileGroups(array &$form, FormStateInterface $form_state) {
    $nbGroups = $form_state->get('nbMobileGroups') + 1;
    $form_state->set('nbMobileGroups', $nbGroups);
    $form_state->setRebuild(TRUE);
  }

  /**
   * Removing the last mobile group.
   */
  public function removeMobileGroups(array &$form, FormStateInterface $form_state) {
    $nbGroups = $form_state->get('nbMobileGroups') - 1;
    $form_state->set('nbMobileGroups', $nbGroups);
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($form_state->getValue('mobile')) {
      // Check that we have the same javascript list in both mobile & desktop.
      $groups = $form_state->getValue('groups');
      $jsDesktop = $this->extractJs($groups);

      $mobileGroups = $form_state->getValue('mobile_groups');
      $jsMobile = $this->extractJs($mobileGroups);

      // Cannot set multiple errors.
      // See https://www.drupal.org/project/drupal/issues/2818437.
      $mobileErrors = [];
      foreach ($jsDesktop as $js) {
        if (!isset($jsMobile[$js])) {
          $mobileErrors[] = $js;
        }
      }

      $globalErrors = [];
      foreach ($jsMobile as $js) {
        if (!isset($jsDesktop[$js])) {
          $globalErrors[] = $js;
        }
      }

      $errorMessage = '';
      if (count($mobileErrors) > 0) {
        $errorMessage .= $this->t('Missing @mobileErrors in the mobile groups.',
        ['@mobileErrors' => implode(", ", $mobileErrors)]);
      }

      if (count($globalErrors) > 0) {
        if (!empty($errorMessage)) {
          $errorMessage .= ' ';
        }
        $errorMessage .= $this->t('Missing @globalErrors in the global groups.',
        ['@globalErrors' => implode(' & ', $globalErrors)]);
      }

      if (!empty($errorMessage)) {
        $form_state->setError($form, $errorMessage);
      }
    }
  }

  /**
   * Return 1 array of all javascripts from 1 group.
   */
  protected function extractJs($groups) {
    $groups = array_slice($groups, 0, count($groups) - 2);

    $globalJsArray = [];
    foreach ($groups as $group) {
      $jsList = explode("\r\n", $group['javascript']);
      foreach ($jsList as $js) {
        if (!empty($js)) {
          $globalJsArray[trim($js)] = trim($js);
        }
      }
    }
    return $globalJsArray;
  }

  /**
   * Overrides Drupal\Core\Entity\EntityFormController::save().
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   An associative array containing the current state of the form.
   */
  public function save(array $form, FormStateInterface $form_state) {
    // EntityForm provides us with the entity we're working on.
    $config = $this->getEntity();

    // Sets default weight on creation.
    if ($config->get('weight') === NULL) {
      $config->set('weight', 0);
    }

    // Unset mobile strategy settings if mobile mode disabled.
    if (!$config->get('mobile')) {
      $config->set('mobile_groups', NULL);
    }

    $status = $config->save();

    if ($status == SAVED_UPDATED) {
      $this->messenger()->addMessage($this->t('Strategy %label has been updated.', ['%label' => $config->label()]));
    }
    else {
      $this->messenger()->addMessage($this->t('Strategy %label has been added.', ['%label' => $config->label()]));
    }

    // Clear Caches.
    Cache::invalidateTags(['library_info']);

    // Redirect the user back to the listing route after the save operation.
    $form_state->setRedirect('entity.js_delay_strategy.collection');
  }

}
