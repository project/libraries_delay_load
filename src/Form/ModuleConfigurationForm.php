<?php

namespace Drupal\libraries_delay_load\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'libraries_delay_load_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'libraries_delay_load.admin_settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('libraries_delay_load.admin_settings');

    $form['enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Load Delay Library?'),
      '#default_value' => $config->get('enabled') ?? 0,
      '#description' => $this->t('Disable for testing purposes.'),
    ];

    $form['debug'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug mode?'),
      '#default_value' => $config->get('debug') ?? 0,
      '#description' => $this->t('Display notices messages the first time a page is loaded to list all the javascripts loaded directly by Drupal. Also output the order of what is loaded in the console'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('libraries_delay_load.admin_settings');
    $config->set('enabled', $form_state->getValue('enabled'));
    $config->set('debug', $form_state->getValue('debug'));

    $config->save();

    // Clear Caches.
    Cache::invalidateTags(['library_info']);

    parent::submitForm($form, $form_state);
  }

}
