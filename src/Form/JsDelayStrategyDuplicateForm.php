<?php

namespace Drupal\libraries_delay_load\Form;

use Drupal\Core\Entity\EntityInterface;

/**
 * Provides the libraries_delay_load pattern duplicate form.
 */
class JsDelayStrategyDuplicateForm extends JsDelayStrategyForm {

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {
    $this->entity = $entity->createDuplicate();
    $this->entity->label = $this->entity->label() . ' - Duplicate';
    return $this;
  }

}
